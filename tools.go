package main

import (
	"log"
	"time"
)

// TrackTime prints message with elapsed time
// Usage: defer TrackTime(time.Now(), "MyFunctionName", "")
func TrackTime(startTime time.Time, title string, domain string) {
	elapsed := time.Since(startTime)
	if domain == "" {
		log.Printf("%s took %s", title, elapsed)
	} else {
		log.Printf("%s for %s took %s", title, domain, elapsed)
	}
}
