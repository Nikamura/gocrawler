package main

import (
	"testing"
)

func assertEqual(t *testing.T, a interface{}, b interface{}) {
	if a == b {
		return
	}
	t.Errorf("%v != %v", a, b)
}

func TestParseHTTPLinks(t *testing.T) {
	pageBody := `
		<a href="news/">
			My Index
		</a>"
		<a href="/about">
			My Index
		</a>"
		<a href="https://www.domain.com/index.html" classes="test">My Index</a>
		<a href="https://domain2.com/index.html" onClick="goTo(A);">My Index</a>
		<a href="https://subdomain.domain.com/about">My Index</a>
		<a href="tel:911">Call police, other protocol</a>
		<a href="tel:911">Call police, other protocol</a>
		https://www.reddit.com/ some other link that's not an anchor tag
	`

	parsedLinks := ParseHTTPLinks(pageBody)

	total := len(parsedLinks.data)

	assertEqual(t, total, 6)
	assertEqual(t, parsedLinks.data["news/"], 1)
	assertEqual(t, parsedLinks.data["/about"], 1)
	assertEqual(t, parsedLinks.data["https://www.domain.com/index.html"], 1)
	assertEqual(t, parsedLinks.data["https://domain2.com/index.html"], 1)
	assertEqual(t, parsedLinks.data["https://subdomain.domain.com/about"], 1)
	assertEqual(t, parsedLinks.data["tel:911"], 2)
}

func TestGroupLinks(t *testing.T) {
	pageBody := `
		<a href="news/">
			My Index
		</a>"
		<a href="/about">
			My Index
		</a>"
		<a href="https://www.domain.com/index.html" classes="test">My Index</a>
		<a href="https://domain2.com/index.html" onClick="goTo(A);">My Index</a>
		<a href="https://subdomain.domain.com/about">My Index</a>
		<a href="tel:911">Call police, other protocol</a>
		<a href="tel:911">Call police, other protocol</a>
		https://www.reddit.com/ some other link that's not an anchor tag
	`
	currentPath := "https://www.domain.com/not-base/"

	parsedLinks := ParseHTTPLinks(pageBody)

	internal, local, external := GroupLinks(currentPath, parsedLinks)

	assertEqual(t, len(internal.data), 3)
	assertEqual(t, len(local.data), 1)
	assertEqual(t, len(external.data), 1)
}
