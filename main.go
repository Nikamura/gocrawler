package main

import (
	"log"
	"time"
)

func main() {
	const domain = "https://monzo.com/" // domain to crawl

	const requestRate = time.Second / 25 // how often crawl a page second/100 = 100 pages per second

	const pageLimit = -1 // limit sitemap size by number of pages it crawls. -1 for unlimited

	const concurrencyCount = 1 // how many sitemap builders to run

	start := time.Now()

	sitemap := GenerateSitemap(domain, pageLimit, requestRate, concurrencyCount)

	elapsed := time.Since(start)

	totalUrls := len(sitemap.urls)

	perSecond := float64(totalUrls) / elapsed.Seconds()

	log.Printf("Sitemap generated, total links %v, elapsed %s, %.1f urls / per second", totalUrls, elapsed, perSecond)

	sitemap.WriteToFile("sitemap.json")
}
