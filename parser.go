package main

import (
	"bytes"
	"log"
	url2 "net/url"
	"regexp"
	"strings"
)

// HTTPLinks stores map of urls and their frequency
type HTTPLinks struct {
	data map[string]int
}

// Set updates map value at `url` location with `count`
func (set *HTTPLinks) Set(url string, count int) {
	set.data[url] = count
}

// Add increments URL frequency by one or adds to the map if doesn't exist
func (set *HTTPLinks) Add(url string) {
	results, ok := set.data[url]
	if ok {
		set.Set(url, results+1)
	} else {
		set.Set(url, 1)
	}
}

// ParseHTTPLinks parses <a href> http(s) urls.
func ParseHTTPLinks(pageBody string) HTTPLinks {
	//defer TrackTime(time.Now(), "ParseHTTPLinks", currentPath)

	// Simple regex to find all <a href=""> in a pageBody
	httpLinkRegex := regexp.MustCompile(`<a.*?href="(.*?)".*?>`)

	links := httpLinkRegex.FindAllStringSubmatch(pageBody, -1)

	allLinks := HTTPLinks{data: make(map[string]int)}

	for _, link := range links {
		url := link[1]
		allLinks.Add(url)
	}

	return allLinks
}

func parseDomain(link *url2.URL) (domain string) {
	split := strings.SplitAfter(link.Host, ".")
	splitSize := len(split)

	if splitSize < 2 {
		return ""
	}

	var buffer bytes.Buffer

	buffer.WriteString(split[splitSize-2])
	buffer.WriteString(split[splitSize-1])

	return buffer.String()
}

// GroupLinks based on their status to currentPath
// Currently only internalLinks working correctly
func GroupLinks(currentPath string, links HTTPLinks) (internalLinks HTTPLinks, localLinks HTTPLinks, externalLinks HTTPLinks) {
	//defer TrackTime(time.Now(), "filterLinks", currentPath)

	currentURL, err := url2.Parse(currentPath)
	if err != nil {
		log.Fatal(err)
	}

	internalLinks.data = make(map[string]int)
	localLinks.data = make(map[string]int)
	externalLinks.data = make(map[string]int)

	for link, count := range links.data {
		if strings.HasPrefix(link, "&") {
			continue
		}

		matchedURL, err := url2.Parse(link)
		if err != nil {
			log.Fatal(err)
		}

		resolvedURL := currentURL.ResolveReference(matchedURL)

		// Ignore all non HTTP(s) links
		if resolvedURL.Scheme != "http" && resolvedURL.Scheme != "https" {
			//log.Printf("Skipping: %s", matchedURL)
			continue
		}

		if resolvedURL.Scheme == currentURL.Scheme &&
			resolvedURL.Host == currentURL.Host {
			//log.Printf("internalLinks: %s", resolvedURL.String())
			internalLinks.Set(resolvedURL.String(), count)
			continue
		}

		if parseDomain(currentURL) == parseDomain(resolvedURL) {
			//log.Printf("localLinks: %s", resolvedURL.String())
			localLinks.Set(resolvedURL.String(), count)
			continue
		} else {
			externalLinks.Set(resolvedURL.String(), count)
			continue
		}
	}

	return internalLinks, localLinks, externalLinks
}
