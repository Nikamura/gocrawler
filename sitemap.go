package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"sync"
	"time"
)

// SitemapURL something
type SitemapURL struct {
	url    string
	status int
	urls   HTTPLinks
}

// Sitemap generated
type Sitemap struct {
	sync.RWMutex
	urls      map[string]SitemapURL
	class     string
	pageLimit int // -1 for unlimited
}

// Add SitemapURL to a Sitemap with write lock
func (sitemap *Sitemap) Add(sitemapURL SitemapURL) {
	sitemap.Lock()
	sitemap.urls[sitemapURL.url] = sitemapURL
	sitemap.Unlock()
}

// IsNewURL checks if URL has been crawled and locks for crawling if it hasn't been crawled
func (sitemap *Sitemap) IsNewURL(url string) bool {
	sitemap.RLock()
	_, ok := sitemap.urls[url]
	if ok {
		sitemap.RUnlock()

		return false
	}

	sitemap.RUnlock()
	sitemap.Lock()
	defer sitemap.Unlock()

	_, ok = sitemap.urls[url]
	if ok {
		return false // just in case
	}

	sitemap.urls[url] = SitemapURL{url: url, status: 0, urls: HTTPLinks{}}

	return true
}

func (sitemap *Sitemap) isLimitReached() bool {
	if sitemap.pageLimit == -1 {
		return false
	}

	return len(sitemap.urls) >= sitemap.pageLimit
}

// WriteToFile writes links data between different pages.
func (sitemap *Sitemap) WriteToFile(fileName string) {
	output := make(map[string][]string)

	for url, data := range sitemap.urls {
		for link := range data.urls.data {
			output[url] = append(output[url], link)
		}
	}

	sitemapJSON, err := json.MarshalIndent(output, "", "\t")
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile(fileName, sitemapJSON, 0644)
	if err != nil {
		log.Fatal(err)
	}
}

// AddFetchResults processed fetchResults based on sitemap type. (only "local" supported)
func (sitemap *Sitemap) AddFetchResults(fetchResults FetchResults) (newLinks []string) {
	if sitemap.class == "local" {
		sitemap.Add(SitemapURL{
			url:    fetchResults.url,
			status: fetchResults.responseCode,
			urls:   fetchResults.internalLinks,
		})

		for link := range fetchResults.internalLinks.data {
			if sitemap.isLimitReached() {
				log.Println("limit has been reached. no new URLs will be crawled")
				break
			}
			if sitemap.IsNewURL(link) {
				newLinks = append(newLinks, link)
			}
		}
	} else {
		log.Fatal("do not know how to build non `local` sitemap")
	}

	return
}

func (sitemap *Sitemap) crawlingURLsCount() (count int) {
	count = 0

	sitemap.RLock()
	for _, link := range sitemap.urls {
		if link.status == 0 {
			count = count + 1
		}
	}
	sitemap.RUnlock()
	return
}

func (sitemap *Sitemap) crawlingURLs() (list []string) {
	sitemap.RLock()
	for _, link := range sitemap.urls {
		if link.status == 0 {
			list = append(list, link.url)
		}
	}
	sitemap.RUnlock()
	return
}

// Read SitemapURL from Sitemap, by url with read lock
func (sitemap *Sitemap) Read(url string) (value SitemapURL, ok bool) {
	sitemap.RLock()
	results, ok := sitemap.urls[url]
	sitemap.RUnlock()
	return results, ok
}

// Build worker for sitemap
func (sitemap *Sitemap) Build(crawl chan<- string, crawled <-chan FetchResults, done chan<- bool) {
	for {
		fetchResults := <-crawled
		newLinks := sitemap.AddFetchResults(fetchResults)
		for _, link := range newLinks {
			crawl <- link
		}

		if len(newLinks) == 0 {
			crawlingCount := sitemap.crawlingURLsCount()
			allFinished := crawlingCount == 0

			if allFinished {
				done <- true
			} else {
				log.Printf("URLs left to crawl: %v", crawlingCount)
			}
		}
	}
}

// GenerateSitemap generates for given domain
func GenerateSitemap(domain string, pageLimit int, requestRate time.Duration, concurrencyCount int) Sitemap {
	defer TrackTime(time.Now(), "GenerateSitemap", domain)

	if concurrencyCount < 1 {
		log.Fatal("concurrencyCount < 1")
	}

	done := make(chan bool, 1)

	crawled := make(chan FetchResults, 100)

	limiter := time.Tick(requestRate)

	var crawler chan string

	sitemap := Sitemap{urls: make(map[string]SitemapURL), class: "local", pageLimit: pageLimit}
	for i := 0; i < concurrencyCount; i++ {
		crawlerName := fmt.Sprintf("crawler%d", i)
		log.Printf("Starting %s", crawlerName)
		crawler = RunCrawler(crawlerName, crawled, limiter)
		go sitemap.Build(crawler, crawled, done)
	}

	crawler <- domain // start generation

	log.Println("Running...")
	<-done

	return sitemap
}
