package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

// FetchResults stores fetched and processed page
type FetchResults struct {
	url           string
	responseCode  int
	internalLinks HTTPLinks
	localLinks    HTTPLinks
	externalLinks HTTPLinks
}

func fetchPage(page string) (string, int) {
	//defer TrackTime(time.Now(), "fetching", page)

	resp, err := http.Get(page)
	if err != nil {
		log.Printf("fetchPage ERROR: %s", err.Error())
		return "", -1
	}

	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	return string(responseBody), resp.StatusCode
}

// Fetch given pageURL and return FetchResults of processed content
func Fetch(workerName string, pageURL string) FetchResults {
	defer TrackTime(time.Now(), workerName, pageURL)

	pageBody, responseCode := fetchPage(pageURL)

	internalLinks, localLinks, externalLinks := GroupLinks(pageURL, ParseHTTPLinks(pageBody))

	return FetchResults{
		url:           pageURL,
		responseCode:  responseCode,
		internalLinks: internalLinks,
		localLinks:    localLinks,
		externalLinks: externalLinks,
	}
}

// RunCrawler starts crawler and returns input channel for it
func RunCrawler(name string, callback chan<- FetchResults, rateLimiter <-chan time.Time) chan string {
	crawlURL := make(chan string, 100)

	go func() {
		for {
			pageURL := <-crawlURL
			go Crawl(name, pageURL, callback, rateLimiter)
		}
	}()

	return crawlURL
}

// Crawl fetches page according to rateLimiter speed and sends processed results to callback
func Crawl(workerName string, pageURL string, callback chan<- FetchResults, rateLimiter <-chan time.Time) {
	log.Printf("%s queueing %s", workerName, pageURL)

	<-rateLimiter

	fetchResults := Fetch(workerName, pageURL)

	callback <- fetchResults
}
